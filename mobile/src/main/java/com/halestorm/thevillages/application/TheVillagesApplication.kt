package com.halestorm.thevillages.application

import android.app.Application
import com.halestorm.thevillages.data.features.dagger.DaggerVillagesComponent
import com.halestorm.thevillages.data.features.dagger.VillagesComponent
import com.halestorm.thevillages.data.features.dagger.VillagesModule

class TheVillagesApplication : Application() {

  companion object {
    lateinit var villagesComponent: VillagesComponent
  }

  override fun onCreate() {
    super.onCreate()
    villagesComponent = DaggerVillagesComponent.builder()
      .villagesModule(VillagesModule(this)).build()
  }
}