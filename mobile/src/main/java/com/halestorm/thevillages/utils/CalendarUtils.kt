package com.halestorm.thevillages.utils

import android.net.Uri
import com.halestorm.thevillages.data.features.retrofit.calendar.CalendarResponse
import com.halestorm.thevillages.data.model.calendar.CalendarEvent
import java.text.SimpleDateFormat
import java.util.*

class CalendarUtils {

  companion object {

    fun buildCalendarEvent(response: CalendarResponse): CalendarEvent {
      val id = response.eventId.toInt()
      val featured = getBooleanValue(response.featuredEvent)
      val title = response.eventTitle
      val description = response.description
      val longDescription = response.longDescription
      val location = response.location
      val startTime = response.startTime
      val endTime = response.endTime
      val startDate = getDate(response.eventDate)
      val allDayEvent = getBooleanValue(response.allDayEvent)
      val website = response.website
      val googleMapsUri = getGoogleMapsUri(response.latitude, response.longitude, response.location)
      val phoneNumber = response.phoneNumber
      val email = response.email

      return CalendarEvent(
        id, featured, title, description, longDescription, location, startDate,
        startTime, endTime, allDayEvent, website, googleMapsUri, phoneNumber, email
      )
    }

    private fun getBooleanValue(input: String) : Boolean {
      if (input == "Y") {
        return true
      } else if (input == "N") {
        return false
      }
      return false
    }

    private fun getDate(input: String) : Date {
      val pattern = "YYYY-MM-dd'T'HH:mm:ss"
      val sdf = SimpleDateFormat(pattern, Locale.US)
      return sdf.parse(input)
    }

    private fun getGoogleMapsUri(latitude: String, longitude: String, location: String) : String {
      return "geo:$latitude,$longitude?q=" + Uri.encode("$location, The Villages")
    }
  }
}