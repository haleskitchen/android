package com.halestorm.thevillages.utils

import android.content.Context
import android.content.SharedPreferences
import org.jetbrains.anko.defaultSharedPreferences

/**
 * Created by joshuahale on 1/31/18.
 */
class SharedPrefsUtils {

  companion object {

    private const val PREF_EULA_ACCEPTED = "com.halestorm.thevillages.EULA_ACCEPTED"
    private const val PREF_SELECTED_AREA = "com.halestorm.thevillages.SELECTED_AREA"
    private const val PREF_SELECTED_AREA_ID = "com.halestorm.thevillages.SELECTED_AREA_ID"
    private const val PREF_SELECTED_MAX_PRICE = "com.halestorm.thevillages.SELECTED_MAX_PRICE"
    private const val PREF_SELECTED_MIN_PRICE = "com.halestorm.thevillages.SELECTED_MIN_PRICE"
    private const val PREF_SHOW_NEW_HOMES = "com.halestorm.thevillages.SHOW_NEW_HOMES"
    private const val PREF_SHOW_PENDING_HOMES = "com.halestorm.thevillages.SHOW_PENDING_HOMES"
    private const val PREF_SHOW_PREOWNED_HOMES = "com.halestorm.thevillages.SHOW_PREOWNED_HOMES"

    fun setEulaAccepted(context: Context, accepted: Boolean) {
      getEditor(context).putBoolean(PREF_EULA_ACCEPTED, accepted).apply()
    }

    fun isEulaAccepted(context: Context): Boolean {
      return getPrefs(context).getBoolean(PREF_EULA_ACCEPTED, false)
    }

    fun setSelectedArea(context: Context, position: Int) {
      getEditor(context).putInt(PREF_SELECTED_AREA, position).apply()
    }

    fun getSelectedArea(context: Context) : Int {
      return getPrefs(context).getInt(PREF_SELECTED_AREA, 0)
    }

    fun setSelectedAreaId(context: Context, position: String) {
      getEditor(context).putString(PREF_SELECTED_AREA_ID, position).apply()
    }

    fun getSelectedAreaId(context: Context) : String {
      return getPrefs(context).getString(PREF_SELECTED_AREA_ID, "1")
    }

    fun setSelectedMaxPrice(context: Context, position: Int) {
      getEditor(context).putInt(PREF_SELECTED_MAX_PRICE, position).apply()
    }

    fun getSelectedMaxPrice(context: Context) : Int {
      return getPrefs(context).getInt(PREF_SELECTED_MAX_PRICE, 0)
    }

    fun setSelectedMinPrice(context: Context, position: Int) {
      getEditor(context).putInt(PREF_SELECTED_MIN_PRICE, position).apply()
    }

    fun getSelectedMinPrice(context: Context) : Int {
      return getPrefs(context).getInt(PREF_SELECTED_MIN_PRICE, -1)
    }

    fun setShowNewHomes(context: Context, showNewHomes : Boolean) {
      getEditor(context).putBoolean(PREF_SHOW_NEW_HOMES, showNewHomes).apply()
    }

    fun showNewHomes(context: Context) : Boolean {
      return getPrefs(context).getBoolean(PREF_SHOW_NEW_HOMES, true)
    }

    fun setShowPendingHomes(context: Context, showPendingHomes: Boolean) {
      getEditor(context).putBoolean(PREF_SHOW_PENDING_HOMES, showPendingHomes).apply()
    }

    fun showPendingHomes(context: Context) : Boolean {
      return getPrefs(context).getBoolean(PREF_SHOW_PENDING_HOMES, true)
    }

    fun setShowPreownedHomes(context: Context, showPreownedHomes: Boolean) {
      getEditor(context).putBoolean(PREF_SHOW_PREOWNED_HOMES, showPreownedHomes).apply()
    }

    fun showPreownedHomes(context: Context) : Boolean {
      return getPrefs(context).getBoolean(PREF_SHOW_PREOWNED_HOMES, true)
    }

    private fun getEditor(context: Context) : SharedPreferences.Editor {
      return getPrefs(context).edit()
    }

    private fun getPrefs(context: Context) : SharedPreferences {
      return context.defaultSharedPreferences
    }
  }
}