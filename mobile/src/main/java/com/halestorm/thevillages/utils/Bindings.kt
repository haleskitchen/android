package com.halestorm.thevillages.utils

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView


/**
 * Created by joshuahale on 1/18/18.
 */
class Bindings {

  companion object {

    @BindingAdapter("adapter")
    public fun setAdapter(recyclerView : RecyclerView, adapter : kotlin.Any) {

      if (adapter is RecyclerView.Adapter<*>) {
        recyclerView.adapter = adapter
      }
    }
  }
}