package com.halestorm.thevillages.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by joshuahale on 1/19/18.
 */
class DateUtils {

  companion object {
    private val fullDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

    fun getDayOfWeek(dayOfWeek: Int) : String {
      when (dayOfWeek) {
        1 -> return "Sunday"
        2 -> return "Monday"
        3 -> return "Tuesday"
        4 -> return "Wednesday"
        5 -> return "Thursday"
        6 -> return "Friday"
        7 -> return "Saturday"
      }
      return ""
    }

    fun getDayOfWeekInt(timestamp : String) : Int {
      val date = fullDate.parse(timestamp)
      val calendar = Calendar.getInstance()
      calendar.time = date
      return calendar.get(Calendar.DAY_OF_WEEK)
    }
  }
}