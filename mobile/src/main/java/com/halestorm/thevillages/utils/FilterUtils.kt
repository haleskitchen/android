package com.halestorm.thevillages.utils

import android.content.Context
import com.halestorm.thevillages.R
import com.halestorm.thevillages.data.model.homefinder.Filters

/**
 * Created by joshuahale on 1/31/18.
 */
class FilterUtils {

  companion object {
    fun getFilters(context: Context) : Filters {
      val showNewHomes = SharedPrefsUtils.showNewHomes(context)
      val showPendingHomes = SharedPrefsUtils.showPendingHomes(context)
      val showPreownedHomes = SharedPrefsUtils.showPreownedHomes(context)

      val area = SharedPrefsUtils.getSelectedAreaId(context)
      val maxPrice = context.resources.
        getStringArray(R.array.max_price_ints)[SharedPrefsUtils.getSelectedMaxPrice(context)]?.toInt()
      val minPrice = context.resources.
        getStringArray(R.array.min_price_ints)[SharedPrefsUtils.getSelectedMinPrice(context)]?.toInt()

      return Filters(area, maxPrice, minPrice, showNewHomes, showPendingHomes, showPreownedHomes)
    }
  }
}