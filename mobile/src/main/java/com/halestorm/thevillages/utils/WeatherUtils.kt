package com.halestorm.thevillages.utils

import android.content.Context
import android.util.Log
import com.halestorm.thevillages.R
import java.text.SimpleDateFormat
import java.util.*

class WeatherUtils {

  companion object {
    private val TAG = WeatherUtils::class.java.simpleName

    fun getDegreesInFahrenheit(kelvin : Float) : Int {
      return ((kelvin - 273) * 9.0 / 5 + 32).toInt()
    }

    fun getWeatherIconResId(iconCode: String, description: String) : Int {
      when (iconCode) {
        "01d" -> return R.drawable.ic_weather_clear
        "02d" -> return R.drawable.ic_weather_clear
        "02n" -> return R.drawable.ic_weather_cloudy
        "03d" -> return R.drawable.ic_weather_mostly_cloudy
        "03n" -> return R.drawable.ic_weather_mostly_sunny
        "04d" -> return R.drawable.ic_weather_mostly_cloudy
        "04n" -> return R.drawable.ic_weather_mostly_cloudy
        "10d" -> return R.drawable.ic_weather_rain
        "10n" -> return R.drawable.ic_weather_rain
        else -> {
          Log.e(TAG , "iconCode $iconCode unknown for $description")
          return R.drawable.ic_weather_unknown
        }
      }
    }

    fun getTimeOfDay(date: String, context: Context) : String {
      val format = SimpleDateFormat("YYYY-MM-dd HH:mm:ss", Locale.getDefault())
      val dateObject = format.parse(date)
      val calendar = Calendar.getInstance()
      calendar.time = dateObject
      var hours = calendar.get(Calendar.HOUR)
      if (hours == 0) {
        hours = 12
      }
      if (calendar.get(Calendar.AM_PM) == Calendar.AM) {
        return String.format(context.getString(R.string.am), hours)
      } else {
        return String.format(context.getString(R.string.pm), hours)
      }
    }
  }
}