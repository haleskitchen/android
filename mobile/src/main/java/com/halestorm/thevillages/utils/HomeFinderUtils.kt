package com.halestorm.thevillages.utils

import java.util.*

class HomeFinderUtils {

  companion object {

    private val HOME_IMAGE_URL = "http://www.thevillages.com/homes/vls/ImageDisplay/ImgDispByKey.ashx?c=%s&u=%s&us=%s&l=%s&w=600&h=300"

    fun getHomeImageUrl(mobileData: List<String>) : String {
      return String.format(HOME_IMAGE_URL, mobileData[1], mobileData[2], mobileData[3], mobileData[4])
    }

    fun getUliKey(mobileData: List<String>) : String {
      return mobileData[0]
    }

    fun getCounty(mobileData: List<String>) : String {
      return mobileData[1]
    }

    fun getUnit(mobileData: List<String>) : String {
      return mobileData[2]
    }

    fun getUnitSub(mobileData: List<String>) : String {
      return mobileData[3]
    }

    fun getLot(mobileData: List<String>) : String {
      return mobileData[4]
    }

    fun getLotSub(mobileData: List<String>) : String {
      return mobileData[5]
    }

    fun getVLSNumber(mobileData: List<String>) : String {
      return mobileData[7]
    }

    fun getId(mobileData: List<String>) : String {
      return mobileData[8]
    }

    fun getDescription(mobileData: List<String>) : String {
      return mobileData[9]
    }

    fun getListingPrice(mobileData: List<String>) : String {
      return mobileData[10]
    }

    fun getVillageName(mobileData: List<String>) : String {
      return mobileData[11]
    }

    fun getStreetAddress(mobileData: List<String>) : String {
      return mobileData[12]
    }

    fun getNumberOfBedrooms(mobileData: List<String>) : Int {
      return mobileData[13].toInt()
    }

    fun getNumberOfBathrooms(mobileData: List<String>) : Double {
      var bathroomString = mobileData[14]

      val containsHalfBath = bathroomString.contains("/", true)
      if (containsHalfBath) {
        bathroomString = bathroomString.split(" ")[0]
        return bathroomString.toDouble() + 0.5
      } else {
        return bathroomString.toDouble()
      }
    }

    fun getSquareFootage(mobileData: List<String>) : Int {
      var squareFootage = mobileData[15]
      squareFootage = squareFootage.replace(",", "")
      return squareFootage.toInt()
    }

    fun getSeriesNumber(mobileData: List<String>) : String {
      return mobileData[18]
    }

    fun getListingDate(mobileData: List<String>) : Date {
      val date = mobileData[22].toLong() * 1000
      return Date(date)
    }

    fun getIsOpenHouse(mobileData: List<String>) : Boolean {
      return getBoolean(mobileData[23])
    }

    fun getIsFurnished(mobileData: List<String>) : Boolean {
      return getBoolean(mobileData[24])
    }

    fun getCurrentPrice(mobileData: List<String>) : String {
      return mobileData[27]
    }

    private fun getBoolean(input: String) : Boolean {
      if (input.contentEquals("N")) {
        return false
      } else if (input.contentEquals("Y")) {
        return true
      }

      return false
    }
  }
}