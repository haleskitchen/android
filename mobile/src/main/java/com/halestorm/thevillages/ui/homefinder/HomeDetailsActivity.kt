package com.halestorm.thevillages.ui.homefinder

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.halestorm.thevillages.R
import com.halestorm.thevillages.application.TheVillagesApplication
import com.halestorm.thevillages.data.features.VillagesProvider
import com.halestorm.thevillages.data.features.retrofit.home.ImagesResponse
import com.halestorm.thevillages.data.model.homefinder.Home
import com.halestorm.thevillages.data.model.homefinder.HomeDetails
import com.halestorm.thevillages.databinding.HomeDetailsActivityBinding
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by joshuahale on 5/10/18.
 */
class HomeDetailsActivity : AppCompatActivity() {

  private val disposable = CompositeDisposable()
  private lateinit var binding : HomeDetailsActivityBinding
  private lateinit var home: Home

  @Inject
  lateinit var villagesProvider: VillagesProvider

  companion object {
    private var EXTRA_HOME = "com.halestorm.thevillages.HOME"

    fun newIntent(context: Context?, home: Home) : Intent {
      val intent = Intent(context, HomeDetailsActivity::class.java)
      val bundle = Bundle()
      bundle.putParcelable(EXTRA_HOME, home)
      intent.putExtras(bundle)
      return intent
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    init()
  }

  private fun init() {
    TheVillagesApplication.villagesComponent.inject(this)
    binding = DataBindingUtil.setContentView(this, R.layout.home_details_activity)
    setupActionBar()
    home = intent.extras.getParcelable(EXTRA_HOME)
  }

  private fun setupActionBar() {
    setSupportActionBar(binding.toolbar)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.title = getString(R.string.home_details)
  }

  private fun showHomeDetails(homeDetails: HomeDetails) {

  }

  private fun showImages(response: ImagesResponse) {

  }

  override fun onResume() {
    super.onResume()
    disposable.add(villagesProvider.getPropertyImages(home)
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::showImages, Throwable::printStackTrace))
  }

  override fun onPause() {
    super.onPause()
    disposable.clear()
  }
}