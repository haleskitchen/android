package com.halestorm.thevillages.ui.weather

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.halestorm.thevillages.R
import com.halestorm.thevillages.data.model.weather.ThreeHourForecast
import com.halestorm.thevillages.utils.WeatherUtils
import kotlinx.android.synthetic.main.item_day_forecast.view.*

/**
 * Created by joshuahale on 1/18/18.
 */
class ForecastAdapter :
  RecyclerView.Adapter<ForecastAdapter.ViewHolder>() {

  private val days = mutableListOf<ThreeHourForecast>()

  fun setDailyForecast(dailyForecast : ArrayList<ThreeHourForecast>) {
    days.clear()
    this.days.addAll(dailyForecast)
    notifyDataSetChanged()
  }

  override fun getItemCount() = days.count()

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.bindForecast(days[position])
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_day_forecast, parent, false)
    return ViewHolder(view)
  }

  class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindForecast(forecast : ThreeHourForecast) {
      with(forecast) {

        itemView.date.text = WeatherUtils.getTimeOfDay(forecast.dateTimestamp, itemView.context)
        itemView.temperature_text_view.text = String.format(itemView.context.getString(R.string.fahrenheit), forecast.temperature)
        itemView.weather_image_view.setImageResource(forecast.weatherResourceId)
      }
    }
  }
}