package com.halestorm.thevillages.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.halestorm.thevillages.ui.legal.EulaActivity
import com.halestorm.thevillages.ui.main.MainActivity
import com.halestorm.thevillages.utils.SharedPrefsUtils

class SplashActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val intent = if (SharedPrefsUtils.isEulaAccepted(this)) {
      Intent(this, MainActivity::class.java)
    } else {
      Intent(this, EulaActivity::class.java)
    }

    startActivity(intent)
    finish()
  }
}
