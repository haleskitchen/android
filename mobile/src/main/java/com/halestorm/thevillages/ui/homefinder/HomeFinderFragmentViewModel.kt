package com.halestorm.thevillages.ui.homefinder

import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.ViewModel
import com.halestorm.thevillages.data.model.homefinder.Filters
import com.halestorm.thevillages.data.model.homefinder.Home

class HomeFinderFragmentViewModel : ViewModel(), Observable {

  private val mCallbacks:
    PropertyChangeRegistry by lazy { PropertyChangeRegistry() }

  @Bindable
  val adapter = HomesAdapter()

  fun setHomes(homes : ArrayList<Home>) {
    adapter.setHomes(homes)
  }

  fun setFilters(filters: Filters) {
    adapter.setFilters(filters)
  }

  fun onHomeClicked() : io.reactivex.Observable<Home> {
    return adapter.onHomeClicked()
  }

  override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
    synchronized(this) {
      mCallbacks.add(callback)
    }
  }

  override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
    synchronized(this) {
      mCallbacks.remove(callback)
    }
  }
}