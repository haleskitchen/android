package com.halestorm.thevillages.ui.legal

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.halestorm.thevillages.R
import com.halestorm.thevillages.application.TheVillagesApplication
import com.halestorm.thevillages.data.features.VillagesProvider
import com.halestorm.thevillages.databinding.EulaActivityBinding
import com.halestorm.thevillages.ui.main.MainActivity
import com.halestorm.thevillages.utils.SharedPrefsUtils
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class EulaActivity : AppCompatActivity() {

  private lateinit var disposable : CompositeDisposable
  private lateinit var binding : EulaActivityBinding

  @Inject
  lateinit var villagesProvider : VillagesProvider

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    TheVillagesApplication.villagesComponent.inject(this)
    binding = DataBindingUtil.setContentView(this, R.layout.eula_activity)

    setupWebView()

    villagesProvider.getEula()
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::showEula, Throwable::printStackTrace)
  }

  override fun onResume() {
    super.onResume()
    disposable = CompositeDisposable()

    disposable.add(RxView.clicks(binding.cancelButton).subscribe{ finish() })
    disposable.add(RxView.clicks(binding.acceptButton).subscribe{ acceptEula() })
  }

  private fun setupWebView() {
    binding.webView.settings.javaScriptEnabled = true
  }

  private fun showEula(eula: String) {
    binding.webView.loadDataWithBaseURL("", eula, "text/html", "UTF-8", "")
  }

  private fun acceptEula() {
    SharedPrefsUtils.setEulaAccepted(this, true)
    val intent = Intent(this, MainActivity::class.java)
    startActivity(intent)
    finish()
  }

  override fun onPause() {
    super.onPause()
    disposable.dispose()
  }
}