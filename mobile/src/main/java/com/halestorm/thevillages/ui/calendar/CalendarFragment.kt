package com.halestorm.thevillages.ui.calendar

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.halestorm.thevillages.application.TheVillagesApplication
import com.halestorm.thevillages.data.features.VillagesProvider
import com.halestorm.thevillages.data.model.calendar.CalendarEvent
import com.halestorm.thevillages.databinding.CalendarFragmentBinding
import com.halestorm.thevillages.ui.base.BaseMainNavigationFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CalendarFragment : BaseMainNavigationFragment() {

  private var disposable = CompositeDisposable()
  private var viewModel : CalendarFragmentViewModel? = null

  @Inject
  lateinit var villagesProvider: VillagesProvider

  companion object {
    fun newInstance() : CalendarFragment {
      return CalendarFragment()
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    TheVillagesApplication.villagesComponent.inject(this)
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                            savedInstanceState: Bundle?): View? {
    val binding = CalendarFragmentBinding.inflate(inflater)
    viewModel = ViewModelProviders.of(this).get(CalendarFragmentViewModel::class.java)
    binding.data = viewModel
    binding.recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    disposable.add(villagesProvider.getCalendar()
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::showCalendarEvents, Throwable::printStackTrace))
    return binding.root
  }

  private fun showCalendarEvents(calendarEvents: ArrayList<CalendarEvent>) {
    viewModel?.setCalendarEvents(calendarEvents)
    viewModel?.onLocateButtonClicked()!!
      .subscribe { uri -> showGeolocation(uri) }
  }

  private fun showGeolocation(uriString: String){
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uriString))
    print(uriString)
    intent.`package` = "com.google.android.apps.maps"
    startActivity(intent)
  }
}