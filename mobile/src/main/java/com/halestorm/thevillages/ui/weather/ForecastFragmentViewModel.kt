package com.halestorm.thevillages.ui.weather

import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.ViewModel
import com.halestorm.thevillages.data.model.weather.ThreeHourForecast

/**
 * Created by joshuahale on 1/18/18.
 */
class ForecastFragmentViewModel : ViewModel(), Observable {

  private val mCallbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }

  @Bindable
  val adapter = ForecastAdapter()

  fun setForecast(dailyForecast : ArrayList<ThreeHourForecast>) {
    adapter.setDailyForecast(dailyForecast)
  }

  override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
    synchronized(this) {
      mCallbacks.add(callback)
    }
  }

  override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
    synchronized(this) {
      mCallbacks.remove(callback)
    }
  }
}