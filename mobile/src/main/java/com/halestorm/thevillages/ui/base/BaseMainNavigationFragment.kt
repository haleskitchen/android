package com.halestorm.thevillages.ui.base

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.halestorm.thevillages.R
import com.halestorm.thevillages.ui.main.MainActivity
import com.halestorm.thevillages.ui.weather.WeatherActivity

open class BaseMainNavigationFragment : Fragment() {

  var menuResId = R.menu.main_menu

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setHasOptionsMenu(true)
  }

  override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    inflater.inflate(menuResId, menu)
    val parent = activity
    if (parent is MainActivity) {
      val temperature = parent.getTemperature()
      menu.findItem(R.id.action_temperature)?.title = temperature.toString() + getString(R.string.degrees)
    }
    super.onCreateOptionsMenu(menu, inflater)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.action_temperature -> showWeatherActivity()
    }
    return super.onOptionsItemSelected(item)
  }

  private fun showWeatherActivity() {
    val parent = activity
    if (parent is MainActivity) {
      val localWeather = parent.getLocalWeather()
      val intent = WeatherActivity.newIntent(parent.applicationContext, localWeather)
      startActivity(intent)
    }
  }
}