package com.halestorm.thevillages.ui.homefinder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.halestorm.thevillages.R
import com.halestorm.thevillages.data.model.homefinder.Filters
import com.halestorm.thevillages.data.model.homefinder.Home
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_home.view.*

/**
 * Created by joshuahale on 1/19/18.
 */
class HomesAdapter : RecyclerView.Adapter<HomesAdapter.ViewHolder>() {

  private val homes = mutableListOf<Home>()
  private lateinit var filters: Filters
  private val filteredHomes = mutableListOf<Home>()
  private val onClickSubject : PublishSubject<Home> = PublishSubject.create()

  fun setHomes(homes : ArrayList<Home>) {
    this.homes.clear()
    this.homes.addAll(homes)
    notifyDataSetChanged()
  }

  fun setFilters(filters: Filters) {
    this.filters = filters
    notifyDataSetChanged()
  }

  override fun getItemCount() : Int {
    filteredHomes.clear()
    for (home in homes) {
      if (homePassesFilters(home)) {
        filteredHomes.add(home)
      }
    }
    return filteredHomes.size
  }

  override fun onBindViewHolder(holder : ViewHolder, position: Int) {
    holder.bindHome(filteredHomes[position])
    holder.itemView.setOnClickListener { onClickSubject.onNext(filteredHomes[position]) }
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_home, parent, false)
    return ViewHolder(view)
  }

  private fun homePassesFilters(home: Home) : Boolean {
    return true
  }

  fun onHomeClicked() : Observable<Home> {
    return onClickSubject
  }

  class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

    fun bindHome(home : Home) {
      with (home) {
        Picasso.get().load(home.imageUrl).into(itemView.home_image_view)
        itemView.address.text = home.streetAddress
        itemView.bathrooms.text = itemView.context.getString(R.string.number_of_bathrooms, home.bathrooms)
        itemView.bedrooms.text = itemView.context.getString(R.string.number_of_bedrooms, home.bedrooms)
        itemView.listing_price.text = home.listingPrice
        itemView.listing_type.text = itemView.context.getString(R.string.listing_type_for_sale)
        itemView.square_footage.text = itemView.context.getString(
          R.string.square_footage, home.squareFootage)
      }
    }
  }
}