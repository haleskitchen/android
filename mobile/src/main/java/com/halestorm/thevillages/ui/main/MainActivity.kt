package com.halestorm.thevillages.ui.main

import android.os.Bundle
import android.view.Gravity
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.halestorm.thevillages.R
import com.halestorm.thevillages.application.TheVillagesApplication
import com.halestorm.thevillages.data.features.VillagesProvider
import com.halestorm.thevillages.data.features.retrofit.calendar.CalendarResponse
import com.halestorm.thevillages.data.features.retrofit.homefilter.FilterResponse
import com.halestorm.thevillages.data.model.weather.LocalWeather
import com.halestorm.thevillages.data.weather.WeatherProvider
import com.halestorm.thevillages.databinding.MainActivityBinding
import com.halestorm.thevillages.helper.FiltersUIHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {

  private var disposable  = CompositeDisposable()
  private var localWeather: LocalWeather? = null
  private var adapter: NavigationPagerAdapter? = null
  private lateinit var binding: MainActivityBinding

  companion object {
    private val TAG = MainActivity::class.java.simpleName
  }

  @Inject
  lateinit var weatherProvider: WeatherProvider

  @Inject
  lateinit var villagesProvider: VillagesProvider

  @Inject
  lateinit var filtersUiHelper: FiltersUIHelper

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    TheVillagesApplication.villagesComponent.inject(this)
    binding = DataBindingUtil.setContentView(this, R.layout.main_activity)
    setSupportActionBar(binding.included?.toolbar)
    supportActionBar?.setDisplayShowTitleEnabled(true)
    setupTabs()
  }

  private fun setupTabs() {
    adapter = NavigationPagerAdapter(supportFragmentManager)
    binding.viewPager.adapter = adapter
    binding.viewPager.addOnPageChangeListener(this)
    binding.tabs.setupWithViewPager(binding.viewPager)
    val tabCount = binding.tabs.tabCount
    for (i : Int in 0..tabCount) {
      binding.tabs.getTabAt(i)?.setIcon(adapter!!.getIcon(i))
    }
    supportActionBar?.title = getString(adapter!!.getActionBarTitle(0))
  }

  override fun onResume() {
    super.onResume()
    disposable.add(weatherProvider.getWeather()
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::showWeather, Throwable::printStackTrace))
    disposable.add(villagesProvider.getHomefinderFilters()
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::updateFilters, Throwable::printStackTrace))
//    disposable.add(villagesProvider.getCalendar()
//      .subscribeOn(Schedulers.io())
//      .observeOn(AndroidSchedulers.mainThread())
//      .subscribe(this::updateCalendar, Throwable::printStackTrace))
  }

  override fun onPause() {
    super.onPause()
    disposable.clear()
  }

  private fun showWeather(localWeather: LocalWeather) {
    this.localWeather = localWeather
    invalidateOptionsMenu()
  }

  private fun updateCalendar(calendarEvents: Array<CalendarResponse>) {

  }

  private fun updateFilters(filters: FilterResponse) {
    filtersUiHelper.updateFilters(filters, binding.filters)
  }

  fun updateFilterDrawerVisibility() {
    if (binding.drawerLayout.isDrawerOpen(Gravity.END)) {
      binding.drawerLayout.closeDrawer(Gravity.END)
    } else {
      binding.drawerLayout.openDrawer(Gravity.END)
    }
  }

  override fun onPageScrollStateChanged(state: Int) { }

  override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) { }

  override fun onPageSelected(position: Int) {
    supportActionBar?.title = getString(adapter!!.getActionBarTitle(position))
  }

  fun getLocalWeather() : LocalWeather? {
    return localWeather
  }

  fun getTemperature() : Int? {
    return localWeather?.temperature
  }
}