package com.halestorm.thevillages.ui.homefinder

import android.os.Bundle
import android.view.*
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.halestorm.thevillages.R
import com.halestorm.thevillages.application.TheVillagesApplication
import com.halestorm.thevillages.data.features.VillagesProvider
import com.halestorm.thevillages.data.model.homefinder.Filters
import com.halestorm.thevillages.data.model.homefinder.Home
import com.halestorm.thevillages.databinding.HomeFinderFragmentBinding
import com.halestorm.thevillages.helper.FiltersUIHelper
import com.halestorm.thevillages.ui.base.BaseMainNavigationFragment
import com.halestorm.thevillages.ui.main.MainActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class HomeFinderFragment : BaseMainNavigationFragment() {

  var disposable  = CompositeDisposable()
  var viewModel : HomeFinderFragmentViewModel? = null

  @Inject
  lateinit var villagesProvider : VillagesProvider

  @Inject
  lateinit var filtersUiHelper : FiltersUIHelper

  companion object {
    fun newInstance() : HomeFinderFragment {
      return HomeFinderFragment()
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    TheVillagesApplication.villagesComponent.inject(this)
    setHasOptionsMenu(true)
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                            savedInstanceState: Bundle?) : View? {
    val binding = HomeFinderFragmentBinding.inflate(inflater)
    viewModel = ViewModelProviders.of(this).get(HomeFinderFragmentViewModel::class.java)
    binding.data = viewModel
    binding.recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    disposable.add(villagesProvider.getAvailableHomes()
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::showHomes, Throwable::printStackTrace))
    disposable.add(filtersUiHelper.onFiltersChanged()
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::updateHomeList, Throwable::printStackTrace))
    disposable.add(viewModel!!.onHomeClicked()
      .subscribeOn(AndroidSchedulers.mainThread())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::showHomeDetails, Throwable::printStackTrace))
    return binding.root
  }

  private fun showHomes(homes : ArrayList<Home>) {
    viewModel?.setHomes(homes)
  }

  private fun updateHomeList(filters: Filters) {
    viewModel?.setFilters(filters)
  }

  private fun showHomeDetails(home: Home) {
    val intent = HomeDetailsActivity.newIntent(context, home)
    startActivity(intent)
  }


  private fun changeFilterVisibility() {
    val parent = activity
    if (parent is MainActivity) {
      parent.updateFilterDrawerVisibility()
    }
  }

  override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    menuResId = R.menu.homefinder_menu
    super.onCreateOptionsMenu(menu, inflater)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item?.itemId) {
      R.id.action_filters -> {
        changeFilterVisibility()
        return true
      }
    }
    return super.onOptionsItemSelected(item)
  }
}