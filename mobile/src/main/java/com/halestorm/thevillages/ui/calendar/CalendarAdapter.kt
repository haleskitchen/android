package com.halestorm.thevillages.ui.calendar

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.halestorm.thevillages.R
import com.halestorm.thevillages.data.model.calendar.CalendarEvent
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_calendar_event.view.*

class CalendarAdapter : RecyclerView.Adapter<CalendarAdapter.ViewHolder>() {

  private val events = mutableListOf<CalendarEvent>()
  private val onClickSubject : PublishSubject<CalendarEvent> = PublishSubject.create()
  private val onEmailClickSubject : PublishSubject<String> = PublishSubject.create()
  private val onPhoneClickSubject: PublishSubject<String> = PublishSubject.create()
  private val onLocateClickSubject: PublishSubject<String> = PublishSubject.create()


  fun setCalendarEvents(calendarEvents : ArrayList<CalendarEvent>) {
    this.events.clear()
    this.events.addAll(calendarEvents)
    notifyDataSetChanged()
  }

  override fun getItemCount() : Int {
    return events.size
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.bindCalendarEvent(events[position])
    holder.itemView.setOnClickListener { onClickSubject.onNext(events[position]) }
    holder.itemView.locate_image_view.setOnClickListener{ onLocateClickSubject.onNext(events[position].geoLocationUri)}
    holder.itemView.call_image_view.setOnClickListener{ onPhoneClickSubject.onNext(events[position].phoneNumber)}
    holder.itemView.email_image_view.setOnClickListener { onEmailClickSubject.onNext(events[position].emailAddress)}
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_calendar_event, parent, false)
    return ViewHolder(view)
  }

  fun onCalendarEventClicked() : Observable<CalendarEvent> {
    return onClickSubject
  }

  fun onLocateButtonClicked() : Observable<String> {
    return onLocateClickSubject
  }

  fun onCallButtonClicked() : Observable<String> {
    return onPhoneClickSubject
  }

  fun onEmailButtonClicked() : Observable<String> {
    return onEmailClickSubject
  }

  class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

    fun bindCalendarEvent(calendarEvent: CalendarEvent) {
      with (calendarEvent) {
        itemView.event_title.text = calendarEvent.title
        if (!TextUtils.isEmpty(calendarEvent.description)) {
          itemView.event_description.visibility = View.VISIBLE
          itemView.event_description.text = calendarEvent.description
        } else {
          itemView.event_description.visibility = View.GONE
        }
        itemView.event_location.text = calendarEvent.location

        if (!TextUtils.isEmpty(calendarEvent.phoneNumber)) {
          itemView.call_image_view.visibility = View.VISIBLE
        } else {
          itemView.call_image_view.visibility = View.INVISIBLE
        }

        if (!TextUtils.isEmpty(calendarEvent.emailAddress)) {
          itemView.email_image_view.visibility = View.VISIBLE
        } else {
          itemView.email_image_view.visibility = View.INVISIBLE
        }

        if (!TextUtils.isEmpty(calendarEvent.geoLocationUri)) {
          itemView.locate_image_view.visibility = View.VISIBLE
        } else {
          itemView.locate_image_view.visibility = View.INVISIBLE
        }
      }
    }
  }
}