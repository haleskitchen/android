package com.halestorm.thevillages.ui.destinations

import com.halestorm.thevillages.ui.base.BaseMainNavigationFragment

class DestinationsFragment : BaseMainNavigationFragment() {

  companion object {
    fun newInstance() : DestinationsFragment {
      return DestinationsFragment()
    }
  }
}