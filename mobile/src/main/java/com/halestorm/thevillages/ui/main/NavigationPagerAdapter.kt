package com.halestorm.thevillages.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.halestorm.thevillages.R
import com.halestorm.thevillages.ui.calendar.CalendarFragment
import com.halestorm.thevillages.ui.destinations.DestinationsFragment
import com.halestorm.thevillages.ui.homefinder.HomeFinderFragment
import com.halestorm.thevillages.ui.video.VideoFragment

class NavigationPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

  private val icons = intArrayOf(
    R.drawable.ic_destinations,
    R.drawable.ic_home_finder,
    R.drawable.ic_calendar,
    R.drawable.ic_video)

  private val pageTitles = intArrayOf(
    R.string.nav_item_destinations,
    R.string.nav_item_homefinder,
    R.string.nav_item_calendar,
    R.string.nav_item_video
  )

  companion object {
    val FRAGMENT_COUNT = 4
  }

  override fun getCount() = FRAGMENT_COUNT

  override fun getItem(position: Int) : Fragment {
   when (position) {
     0 -> return DestinationsFragment.newInstance()
     1 -> return HomeFinderFragment.newInstance()
     2 -> return CalendarFragment.newInstance()
     3 -> return VideoFragment.newInstance()
     else -> return DestinationsFragment.newInstance()
   }
  }

  override fun getPageTitle(position: Int): CharSequence? {
    return ""
  }

  fun getIcon(position: Int) : Int {
    return icons[position]
  }

  fun getActionBarTitle(position: Int) : Int {
    return pageTitles[position]
  }

}