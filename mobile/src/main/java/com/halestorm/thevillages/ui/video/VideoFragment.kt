package com.halestorm.thevillages.ui.video

import com.halestorm.thevillages.ui.base.BaseMainNavigationFragment

/**
 * Created by joshuahale on 1/20/18.
 */
class VideoFragment : BaseMainNavigationFragment() {

  companion object {
    fun newInstance() : VideoFragment {
      return VideoFragment()
    }
  }
}