package com.halestorm.thevillages.ui.weather

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import androidx.databinding.DataBindingUtil
import com.halestorm.thevillages.R
import com.halestorm.thevillages.application.TheVillagesApplication
import com.halestorm.thevillages.data.model.weather.LocalWeather
import com.halestorm.thevillages.data.model.weather.WeekForecast
import com.halestorm.thevillages.data.weather.WeatherProvider
import com.halestorm.thevillages.databinding.WeatherActivityBinding
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class WeatherActivity : AppCompatActivity() {

  private val disposable = CompositeDisposable()
  private lateinit var binding : WeatherActivityBinding

  @Inject
  lateinit var weatherProvider: WeatherProvider

  companion object {
    private var EXTRA_LOCAL_WEATHER: String = "com.halestorm.thevillages.LOCAL_WEATHER"

    fun newIntent(context: Context, localWeather: LocalWeather?): Intent {
      val intent = Intent(context, WeatherActivity::class.java)
      val bundle = Bundle()
      bundle.putParcelable(EXTRA_LOCAL_WEATHER, localWeather)
      intent.putExtras(bundle)
      return intent
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    init()
  }

  private fun init() {
    TheVillagesApplication.villagesComponent.inject(this)
    binding = DataBindingUtil.setContentView(this, R.layout.weather_activity)
    setupActionBar(binding)
  }

  private fun setupActionBar(binding : WeatherActivityBinding) {
    setSupportActionBar(binding.included?.toolbar)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.title = getString(R.string.activity_weather)
  }

  override fun onResume() {
    super.onResume()
    disposable.add(weatherProvider.getFiveDayForecast()
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::showForecast, Throwable::printStackTrace))
  }

  private fun showForecast(forecast : WeekForecast) {
    val adapter = DailyForecastPagerAdapter(supportFragmentManager, forecast)
    binding.viewPager.adapter = adapter
    binding.tabs.setupWithViewPager(binding.viewPager)
  }

  override fun onPause() {
    super.onPause()
    disposable.clear()
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    when (item?.itemId) {
      android.R.id.home -> {
        NavUtils.navigateUpFromSameTask(this)
        return true
      }
    }
    return super.onOptionsItemSelected(item)
  }
}