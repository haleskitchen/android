package com.halestorm.thevillages.ui.calendar
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.ViewModel
import com.halestorm.thevillages.data.model.calendar.CalendarEvent

class CalendarFragmentViewModel : ViewModel(), Observable {

  private val mCallbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }

  @Bindable
  val adapter = CalendarAdapter()

  fun setCalendarEvents(calendarEvents: ArrayList<CalendarEvent>) {

    adapter.setCalendarEvents(calendarEvents)
  }

  fun onCalendarEventClicked() : io.reactivex.Observable<CalendarEvent> {
    return adapter.onCalendarEventClicked()
  }

  fun onCallButtonClicked() : io.reactivex.Observable<String> {
    return adapter.onCallButtonClicked()
  }

  fun onEmailButtonClicked() : io.reactivex.Observable<String> {
    return adapter.onEmailButtonClicked()
  }

  fun onLocateButtonClicked() : io.reactivex.Observable<String> {
    return adapter.onLocateButtonClicked()
  }

  override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    synchronized(this) {
      mCallbacks.remove(callback)
    }
  }

  override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    synchronized(this) {
      mCallbacks.add(callback)
    }
  }
}