package com.halestorm.thevillages.ui.weather

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.halestorm.thevillages.data.model.weather.WeekForecast

/**
 * Created by joshuahale on 1/19/18.
 */
class DailyForecastPagerAdapter(fm: FragmentManager, private val forecast: WeekForecast) : FragmentPagerAdapter(fm) {

  override fun getCount() = forecast.days.size

  override fun getItem(position: Int): Fragment {
    return ForecastFragment.newInstance(forecast.days[position])
  }

  override fun getPageTitle(position: Int): CharSequence? {
    return forecast.days[position].dayOfWeek
  }
}