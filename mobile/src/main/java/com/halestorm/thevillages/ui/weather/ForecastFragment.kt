package com.halestorm.thevillages.ui.weather

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.halestorm.thevillages.data.model.weather.Day
import com.halestorm.thevillages.data.model.weather.ThreeHourForecast
import com.halestorm.thevillages.databinding.ForecastFragmentBinding

/**
 * Created by joshuahale on 1/19/18.
 */
class ForecastFragment : Fragment() {

  companion object {
    private const val ARG_TITLE = "com.halestorm.thevillages.ARG_TITLE"
    private const val ARG_DAILY_FORECAST = "com.halestorm.thevillages.ARG_DAILY_FORECAST"

    fun newInstance(day : Day): ForecastFragment {
      val extras = Bundle()
      extras.putString(ARG_TITLE, day.dayOfWeek)
      extras.putSerializable(ARG_DAILY_FORECAST, day.threeHourForecast)
      val forecastFragment = ForecastFragment()
      forecastFragment.arguments = extras
      return forecastFragment
    }
  }

  @Suppress("UNCHECKED_CAST")
  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    val binding = ForecastFragmentBinding.inflate(inflater)
    val viewModel = ViewModelProviders.of(this).get(ForecastFragmentViewModel::class.java)
    val forecast = arguments?.getSerializable(ARG_DAILY_FORECAST) as ArrayList<ThreeHourForecast>
    binding.data = viewModel
    viewModel.setForecast(forecast)
    return binding.root
  }
}