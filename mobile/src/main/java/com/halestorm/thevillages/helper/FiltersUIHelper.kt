package com.halestorm.thevillages.helper

import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import com.halestorm.thevillages.R
import com.halestorm.thevillages.data.features.retrofit.homefilter.FilterResponse
import com.halestorm.thevillages.data.model.homefinder.Filters
import com.halestorm.thevillages.databinding.FiltersBinding
import com.halestorm.thevillages.utils.FilterUtils
import com.halestorm.thevillages.utils.SharedPrefsUtils
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * Created by joshuahale on 1/26/18.
 */
class FiltersUIHelper(private var context: Context) : CompoundButton.OnCheckedChangeListener, AdapterView.OnItemSelectedListener {

  private var filters: FilterResponse? = null
  private var binding : FiltersBinding? = null

  private var filtersChangedSubject : PublishSubject<Filters> = PublishSubject.create()

  fun updateFilters(filters: FilterResponse, binding: FiltersBinding?) {
    this.binding = binding
    this.filters = filters

    updateValues()
  }

  private fun updateValues() {
    updateSpinners()
    updateHomeTypeSwitches()
  }

  private fun updateHomeTypeSwitches() {
    binding?.newHomesSwitch?.isChecked = SharedPrefsUtils.showNewHomes(context)
    binding?.pendingHomesSwitch?.isChecked = SharedPrefsUtils.showPendingHomes(context)
    binding?.preownedHomesSwitch?.isChecked = SharedPrefsUtils.showPreownedHomes(context)

    binding?.newHomesSwitch?.setOnCheckedChangeListener(this)
    binding?.pendingHomesSwitch?.setOnCheckedChangeListener(this)
    binding?.preownedHomesSwitch?.setOnCheckedChangeListener(this)
  }

  private fun updateSpinners() {
    binding?.areaSpinner?.adapter = getAreaAdapter()
    binding?.maxPriceSpinner?.adapter = getMaxPriceAdapter()
    binding?.minPriceSpinner?.adapter = getMinPriceAdapter()

    binding?.areaSpinner?.setSelection(SharedPrefsUtils.getSelectedArea(context))
    binding?.maxPriceSpinner?.setSelection(SharedPrefsUtils.getSelectedMaxPrice(context))
    binding?.minPriceSpinner?.setSelection(SharedPrefsUtils.getSelectedMinPrice(context))

    binding?.areaSpinner?.onItemSelectedListener = this
    binding?.maxPriceSpinner?.onItemSelectedListener = this
    binding?.minPriceSpinner?.onItemSelectedListener = this
  }

  private fun getAreaAdapter() : ArrayAdapter<String> {
    val areaStrings = ArrayList<String>()
    areaStrings.add(context.getString(R.string.all_areas))
    filters?.let {
      it.areas.mapTo(areaStrings) { it.name }
    }
    return ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, areaStrings)
  }

  private fun getMaxPriceAdapter() : ArrayAdapter<String> {
    val maxPrices = context.resources.getStringArray(R.array.max_prices)
    return ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, maxPrices)
  }

  private fun getMinPriceAdapter() : ArrayAdapter<String> {
    val minPrices = context.resources.getStringArray(R.array.min_prices)
    return ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, minPrices)
  }

  override fun onCheckedChanged(button: CompoundButton?, value: Boolean) {
    when (button?.id) {
      R.id.new_homes_switch -> SharedPrefsUtils.setShowNewHomes(context, value)
      R.id.pending_homes_switch -> SharedPrefsUtils.setShowPendingHomes(context, value)
      R.id.preowned_homes_switch -> SharedPrefsUtils.setShowPreownedHomes(context, value)
    }
    broadcastFilterUpdate()
  }

  private fun broadcastFilterUpdate() {
    filtersChangedSubject.onNext(FilterUtils.getFilters(context))
  }

  override fun onNothingSelected(p0: AdapterView<*>?) { }

  override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
   when(parent?.id) {
     R.id.area_spinner -> {
       SharedPrefsUtils.setSelectedArea(context, position)
       val areaId = filters?.areas?.get(position)?.id!!
       SharedPrefsUtils.setSelectedAreaId(context, areaId)
     }
     R.id.max_price_spinner -> SharedPrefsUtils.setSelectedMaxPrice(context, position)
     R.id.min_price_spinner -> SharedPrefsUtils.setSelectedMinPrice(context, position)
   }
    broadcastFilterUpdate()
  }

  fun onFiltersChanged() : Observable<Filters> {
    return filtersChangedSubject
  }
}