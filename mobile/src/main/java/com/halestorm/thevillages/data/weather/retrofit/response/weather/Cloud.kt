package com.halestorm.thevillages.data.weather.retrofit.response.weather

import com.google.gson.annotations.SerializedName

data class Cloud (
  @SerializedName("all") val percentage : Int
)