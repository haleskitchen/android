package com.halestorm.thevillages.data.weather.retrofit.response.weather

import com.google.gson.annotations.SerializedName

data class WeatherData (
  @SerializedName("country") val country : String,
  @SerializedName("id") val id : Int,
  @SerializedName("message") val message : Float,
  @SerializedName("sunrise") val sunriseInMillis : Long,
  @SerializedName("sunset") val sunsetInMillis : Long,
  @SerializedName("type") val type : Int
)