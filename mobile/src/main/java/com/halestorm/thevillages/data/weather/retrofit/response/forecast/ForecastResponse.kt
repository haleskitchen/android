package com.halestorm.thevillages.data.weather.retrofit.response.forecast

import com.google.gson.annotations.SerializedName

data class ForecastResponse (
  @SerializedName("list") val dayEntries: List<DayEntry>
)