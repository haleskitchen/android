package com.halestorm.thevillages.data.features.dagger

import com.halestorm.thevillages.ui.calendar.CalendarFragment
import com.halestorm.thevillages.ui.homefinder.HomeDetailsActivity
import com.halestorm.thevillages.ui.homefinder.HomeFinderFragment
import com.halestorm.thevillages.ui.legal.EulaActivity
import com.halestorm.thevillages.ui.main.MainActivity
import com.halestorm.thevillages.ui.weather.WeatherActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(VillagesModule::class)])
interface VillagesComponent {
  fun inject(calendarFragment: CalendarFragment)
  fun inject(eulaActivity: EulaActivity)
  fun inject(mainActivity: MainActivity)
  fun inject(weatherActivity: WeatherActivity)
  fun inject(homeDetailsActivity: HomeDetailsActivity)
  fun inject(homeFinderFragment: HomeFinderFragment)
}