package com.halestorm.thevillages.data.features.retrofit.home

import com.google.gson.annotations.SerializedName

/**
 * Created by joshuahale on 1/19/18.
 */
data class HomeResponse (

  @SerializedName("Data") val homeDataList : List<HomeData>
)