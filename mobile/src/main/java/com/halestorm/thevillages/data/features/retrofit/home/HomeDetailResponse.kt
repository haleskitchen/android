package com.halestorm.thevillages.data.features.retrofit.home

import com.google.gson.annotations.SerializedName

/**
 * Created by joshuahale on 2/8/18.
 */
data class HomeDetailResponse(
  @SerializedName("Data") val homeDataList : List<HomeData>
)
