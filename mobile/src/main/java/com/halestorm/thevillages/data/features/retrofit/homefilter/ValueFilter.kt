package com.halestorm.thevillages.data.features.retrofit.homefilter

import com.google.gson.annotations.SerializedName

/**
 * Created by joshuahale on 1/24/18.
 */
data class ValueFilter (
  @SerializedName("value") val value: Int,
  @SerializedName("display") val display: String
)