package com.halestorm.thevillages.data.weather.retrofit.response.weather

import com.google.gson.annotations.SerializedName

data class Main (
  @SerializedName("humidity") val humidity : Int,
  @SerializedName("pressure") val pressure : Float,
  @SerializedName("temp") val temperature : Float,
  @SerializedName("temp_max") val maxTemperature : Float,
  @SerializedName("temp_min") val minTemperature : Float
)