package com.halestorm.thevillages.data.weather.retrofit.response.weather

import com.google.gson.annotations.SerializedName

data class Weather (
  @SerializedName("description") val description : String,
  @SerializedName("icon") val iconId : String,
  @SerializedName("id") val id : Int,
  @SerializedName("main") val type : String
)
