package com.halestorm.thevillages.data.features.dagger

import android.content.Context
import com.halestorm.thevillages.data.features.VillagesProvider
import com.halestorm.thevillages.data.weather.WeatherProvider
import com.halestorm.thevillages.helper.FiltersUIHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class VillagesModule(private var context: Context) {

  @Provides
  @Singleton
  fun provideWeatherProvider(): WeatherProvider = WeatherProvider()

  @Provides
  @Singleton
  fun provideVillagesProvider() : VillagesProvider = VillagesProvider()

  @Provides
  @Singleton
  fun provideFiltersHelper() : FiltersUIHelper = FiltersUIHelper(context)
}