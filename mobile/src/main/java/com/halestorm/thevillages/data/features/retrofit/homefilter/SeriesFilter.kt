package com.halestorm.thevillages.data.features.retrofit.homefilter

import com.google.gson.annotations.SerializedName

/**
 * Created by joshuahale on 1/24/18.
 */
data class SeriesFilter (
  @SerializedName("title") val title : String,
  @SerializedName("options") val options : ArrayList<OptionFilter>
)