package com.halestorm.thevillages.data.weather.retrofit.response.weather

import com.google.gson.annotations.SerializedName

data class Wind (
  @SerializedName("degree") val degree : Int,
  @SerializedName("speed") val speed : Float
)
