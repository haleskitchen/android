package com.halestorm.thevillages.data.model.calendar

import java.util.*

data class CalendarEvent(
  val id : Int,
  val featured: Boolean,
  val title: String,
  val description: String,
  val longDescription: String,
  val location: String,
  val startDate: Date,
  val startTime: String,
  val endTime: String,
  val allDayEvent: Boolean,
  val website: String,
  val geoLocationUri: String,
  val phoneNumber: String,
  val emailAddress: String
)