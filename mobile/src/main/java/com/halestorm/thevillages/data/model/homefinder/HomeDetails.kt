package com.halestorm.thevillages.data.model.homefinder

/**
 * Created by joshuahale on 5/10/18.
 */
data class HomeDetails(val address: String)