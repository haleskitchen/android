package com.halestorm.thevillages.data.features.retrofit.calendar

import com.google.gson.annotations.SerializedName

data class CalendarResponse (
  @SerializedName("EventID") val eventId: String,
  @SerializedName("FeaturedEvent") val featuredEvent: String,
  @SerializedName("EventTitle") val eventTitle: String,
  @SerializedName("Description") val description: String,
  @SerializedName("LongDescription") val longDescription: String,
  @SerializedName("Location") val location: String,
  @SerializedName("StartTime") val startTime: String,
  @SerializedName("EndTime") val endTime: String,
  @SerializedName("EventDate") val eventDate: String,
  @SerializedName("AllDayEvent") val allDayEvent: String,
  @SerializedName("Date") val date: String,
  @SerializedName("Website") val website: String,
  @SerializedName("GISLatitude") val latitude: String,
  @SerializedName("GISLongitude") val longitude: String,
  @SerializedName("ContactPhone") val phoneNumber: String,
  @SerializedName("ContactEmail") val email: String

  )