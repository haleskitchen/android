package com.halestorm.thevillages.data.features

import com.halestorm.thevillages.data.ResponseTranslator
import com.halestorm.thevillages.data.features.retrofit.VillagesApi
import com.halestorm.thevillages.data.features.retrofit.calendar.CalendarResponse
import com.halestorm.thevillages.data.features.retrofit.home.ImagesResponse
import com.halestorm.thevillages.data.features.retrofit.homefilter.FilterResponse
import com.halestorm.thevillages.data.model.calendar.CalendarEvent
import com.halestorm.thevillages.data.model.homefinder.Home
import com.halestorm.thevillages.data.model.homefinder.HomeDetails
import io.reactivex.Observable

/**
 * Created by joshuahale on 1/19/18.
 */
class VillagesProvider {

  private val villagesApi by lazy {
    VillagesApi.create()
  }

  companion object {
    private const val TAG = "VillagesProvider"
  }

  fun getHomeDetails(uliKey: String) : Observable<HomeDetails> {
    val response = villagesApi.getHomeDetails(uliKey, VillagesApi.HOME_DETAIL_VIEW_LIST,
      VillagesApi.getUliKeyArrayString(), VillagesApi.HOME_DETAIL_SORT,
      VillagesApi.HOME_DETAIL_PAGE, VillagesApi.HOME_DETAIL_PAGE_LENGTH,
      VillagesApi.HOME_DETAIL_KEYWORDS)
    return response.map {
      response -> ResponseTranslator.getHomeDetails(response)
    }
  }

  fun getAvailableHomes() : Observable<ArrayList<Home>> {
    val homesResponse = villagesApi.getHomes()
    return homesResponse.map {
      response -> ResponseTranslator.getHomes(response)
    }
  }

  fun getCalendar() : Observable<ArrayList<CalendarEvent>> {
    val calendarResponse = villagesApi.getCalendar("0", "0", "0", "", "", "0001-01-01", "0001-01-01", "0", "30", "00000000000000000000000000")
    return calendarResponse.map {
      response -> ResponseTranslator.getCalendarEvents(response)
    }
  }

  fun getEula() : Observable<String> {
    val eulaResponse = villagesApi.getEula()
    return eulaResponse.map {
      response -> ResponseTranslator.getEula(response)
    }
  }

  fun getHomefinderFilters() : Observable<FilterResponse> {
    val filtersResponse = villagesApi.getHomefinderFilters()
    return filtersResponse.map {
      response -> ResponseTranslator.getFilters(response)
    }
  }

  fun getPropertyImages(home: Home) : Observable<ImagesResponse> {
    return villagesApi.getPropertyImages(home.county, home.unit, home.unitSub, home.lot, home.lotSub, home.id)
  }
}