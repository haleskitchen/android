package com.halestorm.thevillages.data.features.retrofit.homefilter

import com.google.gson.annotations.SerializedName

/**
 * Created by joshuahale on 1/24/18.
 */
data class OptionFilter(
  @SerializedName("id") val id : String,
  @SerializedName("name") val name : String
)
