package com.halestorm.thevillages.data.features.retrofit.homefilter

import com.google.gson.annotations.SerializedName

data class FilterResponse (
  @SerializedName("villages") val villages : ArrayList<VillageFilter>,
  @SerializedName("familyHoods") val familyNeighborhoods : ArrayList<VillageFilter>,
  @SerializedName("areas") val areas: ArrayList<OptionFilter>,
  @SerializedName("series") val series: ArrayList<SeriesFilter>,
  @SerializedName("seriesNew") val seriesNew: ArrayList<OptionFilter>,
  @SerializedName("bedrooms") val bedroomFilters: ArrayList<ValueFilter>,
  @SerializedName("bathrooms") val bathroomFiltrs: ArrayList<ValueFilter>,
  @SerializedName("garage") val garageFilters: ArrayList<ValueFilter>,
  @SerializedName("homesite") val homesiteFilters: ArrayList<ValueFilter>
)