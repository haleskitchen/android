package com.halestorm.thevillages.data.weather.retrofit.response.weather

import com.google.gson.annotations.SerializedName

data class Snow (
  @SerializedName("3h") val snowfallLastThreeHours : Int
)