package com.halestorm.thevillages.data.model.weather

import java.io.Serializable

/**
 * Created by joshuahale on 1/19/18.
 */
data class ThreeHourForecast (
  var temperature : Int,
  var dateTimestamp : String,
  var weatherResourceId : Int,
  var weatherDescription : String
) : Serializable