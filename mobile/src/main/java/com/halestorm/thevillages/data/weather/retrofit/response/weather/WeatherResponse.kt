package com.halestorm.thevillages.data.weather.retrofit.response.weather

import com.google.gson.annotations.SerializedName

data class WeatherResponse (
  @SerializedName("base") val base : String,
  @SerializedName("clouds") val cloud : Cloud,
  @SerializedName("coord") val coordinates : Coordinates,
  @SerializedName("dt") val dateTimeCalculation : Long,
  @SerializedName("int") val id : Int,
  @SerializedName("main") val main : Main,
  @SerializedName("name") val name : String,
  @SerializedName("rain") val rain : Rain,
  @SerializedName("snow") val snow : Snow,
  @SerializedName("sys") val system : WeatherData,
  @SerializedName("visibility") val visibility : Int,
  @SerializedName("weather") val weather : List<Weather>,
  @SerializedName("wind") val wind : Wind
)