package com.halestorm.thevillages.data.model.homefinder

/**
 * Created by joshuahale on 1/31/18.
 */
data class Filters (
  var areaId: String,
  var maxPrice: Int?,
  var minPrice: Int?,
  var showPendingHomes: Boolean,
  var showPreownedHomes: Boolean,
  var showNewHomes: Boolean
)