package com.halestorm.thevillages.data.weather.retrofit.response.weather

import com.google.gson.annotations.SerializedName

data class Rain (
  @SerializedName("3h") val rainfallPreviousThreeHours : Int
)