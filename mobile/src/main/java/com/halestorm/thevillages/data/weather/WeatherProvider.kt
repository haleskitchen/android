package com.halestorm.thevillages.data.weather

import com.halestorm.thevillages.data.ResponseTranslator
import com.halestorm.thevillages.data.model.weather.LocalWeather
import com.halestorm.thevillages.data.model.weather.WeekForecast
import com.halestorm.thevillages.data.weather.retrofit.WeatherApi
import io.reactivex.Observable

/**
 * Created by joshuahale on 1/11/18.
 */
class WeatherProvider {

  companion object {
    const val villagesId = 4175179
  }

  private val weatherApi by lazy {
    WeatherApi.create()
  }

  fun getWeather() : Observable<LocalWeather> {
    val weatherResponse = weatherApi.getWeather(villagesId)
    return weatherResponse
      .map { response -> ResponseTranslator.getLocalWeather(response) }
  }

  fun getFiveDayForecast() : Observable<WeekForecast> {
    val forecastResponse = weatherApi.getFiveDayForecast(villagesId)
    return forecastResponse
      .map { response -> ResponseTranslator.getForecast(response) }
  }
}