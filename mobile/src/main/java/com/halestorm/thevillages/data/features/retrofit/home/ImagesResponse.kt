package com.halestorm.thevillages.data.features.retrofit.home

data class ImagesResponse (val url: String)