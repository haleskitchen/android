package com.halestorm.thevillages.data.weather.retrofit.response.weather

import com.google.gson.annotations.SerializedName

data class Coordinates (
  @SerializedName("lat") val latitude : Float,
  @SerializedName("lon") val longitude : Float
)