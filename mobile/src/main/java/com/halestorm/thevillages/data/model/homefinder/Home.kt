package com.halestorm.thevillages.data.model.homefinder

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

//
//@Parcelize
//data class Home (val imageUrl : String,
//                 val bedrooms : Int,
//                 val bathrooms : String,
//                 var listingPrice : String,
//                 val squareFootage : String,
//                 val streetAddress : String,
//                 var villageName : String,
//                 var uliKey: String,
//                 var homeType: HomeType) : Parcelable

@Parcelize
data class Home (val imageUrl: String,
                 val uliKey: String,
                 val county: String,
                 val unit: String,
                 val unitSub: String,
                 val lot: String,
                 val lotSub: String,
                 val vlsNumber: String,
                 val id: String,
                 val description: String,
                 val listingPrice: String,
                 val villageName: String,
                 val streetAddress: String,
                 val bedrooms: Int,
                 val bathrooms: Double,
                 val squareFootage: Int,
                 val seriesNumber: String,
                 val listingDate: Date,
                 val openHouse: Boolean,
                 val furnished: Boolean,
                 val currentPrice: String,
                 val homeType: HomeType) : Parcelable