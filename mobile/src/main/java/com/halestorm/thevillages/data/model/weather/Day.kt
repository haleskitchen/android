package com.halestorm.thevillages.data.model.weather

data class Day (
  var threeHourForecast: ArrayList<ThreeHourForecast>,
  var dayOfWeek : String
)