package com.halestorm.thevillages.data.weather.retrofit.response.forecast

import com.google.gson.annotations.SerializedName
import com.halestorm.thevillages.data.weather.retrofit.response.weather.Cloud
import com.halestorm.thevillages.data.weather.retrofit.response.weather.Main
import com.halestorm.thevillages.data.weather.retrofit.response.weather.Weather
import com.halestorm.thevillages.data.weather.retrofit.response.weather.Wind

data class DayEntry(
  @SerializedName("clouds") val clouds: Cloud,
  @SerializedName("dt") val timestamp: Long,
  @SerializedName("dt_txt") val timestampText : String,
  @SerializedName("main") val main : Main,
  @SerializedName("weather") val weather: List<Weather>,
  @SerializedName("wind") val wind: Wind
)