package com.halestorm.thevillages.data.features.retrofit.homefilter

import com.google.gson.annotations.SerializedName

/**
 * Created by joshuahale on 1/24/18.
 */
data class VillageFilter (
  @SerializedName("name") val name : String,
  @SerializedName("area") val area : Int
)