package com.halestorm.thevillages.data

import com.halestorm.thevillages.data.features.retrofit.EulaResponse
import com.halestorm.thevillages.data.features.retrofit.calendar.CalendarResponse
import com.halestorm.thevillages.data.features.retrofit.home.HomeDetailResponse
import com.halestorm.thevillages.data.features.retrofit.home.HomeResponse
import com.halestorm.thevillages.data.features.retrofit.homefilter.FilterResponse
import com.halestorm.thevillages.data.model.calendar.CalendarEvent
import com.halestorm.thevillages.data.model.homefinder.Home
import com.halestorm.thevillages.data.model.homefinder.HomeDetails
import com.halestorm.thevillages.data.model.homefinder.HomeType
import com.halestorm.thevillages.data.model.weather.Day
import com.halestorm.thevillages.data.model.weather.LocalWeather
import com.halestorm.thevillages.data.model.weather.ThreeHourForecast
import com.halestorm.thevillages.data.model.weather.WeekForecast
import com.halestorm.thevillages.data.weather.retrofit.response.forecast.ForecastResponse
import com.halestorm.thevillages.data.weather.retrofit.response.weather.WeatherResponse
import com.halestorm.thevillages.utils.CalendarUtils
import com.halestorm.thevillages.utils.DateUtils
import com.halestorm.thevillages.utils.HomeFinderUtils
import com.halestorm.thevillages.utils.WeatherUtils
import java.util.*


class ResponseTranslator {

  companion object {

    fun getCalendarEvents(calendarResponses : Array<CalendarResponse>) : ArrayList<CalendarEvent> {
      val arrayList = ArrayList<CalendarEvent>()
      for (response in calendarResponses) {
          arrayList.add(CalendarUtils.buildCalendarEvent(response))
      }
      return arrayList
    }

    fun getEula(response: EulaResponse) : String {
      return response.agreement
    }

    fun getForecast(response: ForecastResponse): WeekForecast {
      val daysMap = mutableMapOf<Int, ArrayList<ThreeHourForecast>>()
      for (dayEntry in response.dayEntries) {

        val dayOfWeek = DateUtils.getDayOfWeekInt(dayEntry.timestampText)

        val temperature = WeatherUtils.getDegreesInFahrenheit(dayEntry.main.temperature)
        val dateTimestamp = dayEntry.timestampText
        val weatherDescription = dayEntry.weather[0].description
        val weatherIcon = WeatherUtils.getWeatherIconResId(dayEntry.weather[0].iconId, weatherDescription)
        val threeHourForecast = ThreeHourForecast(temperature, dateTimestamp, weatherIcon,
          weatherDescription)

        var forecasts = daysMap[dayOfWeek]

        if (forecasts == null) {
          forecasts = ArrayList()
        }

        forecasts.add(threeHourForecast)
        daysMap.put(dayOfWeek, forecasts)
      }
      val days = ArrayList<Day>()

      for (entry in daysMap) {
        days.add(Day(entry.value, DateUtils.getDayOfWeek(entry.key)))
      }

      return WeekForecast(days)
    }

    fun getLocalWeather(response: WeatherResponse): LocalWeather {
      val maxTemperature = WeatherUtils.getDegreesInFahrenheit(response.main.maxTemperature)
      val minTemperature = WeatherUtils.getDegreesInFahrenheit(response.main.maxTemperature)
      val name = response.name
      val sunriseInMillis = response.system.sunriseInMillis
      val sunsetInMillis = response.system.sunsetInMillis
      val temperature = WeatherUtils.getDegreesInFahrenheit(response.main.temperature)
      return LocalWeather(maxTemperature, minTemperature, name,
        sunriseInMillis, sunsetInMillis, temperature)
    }

    fun getHomes(response: HomeResponse) : ArrayList<Home> {
      val mobileDataList = response.homeDataList[0].mobileDataList
      val homeList = ArrayList<Home>()

      for (mobileData in mobileDataList) {
        var homeType = HomeType.NEW

        if (HomeFinderUtils.getVLSNumber(mobileData).contentEquals("P")) {
          homeType = HomeType.PREOWNED
        }

        val home = Home(
          HomeFinderUtils.getHomeImageUrl(mobileData),
          HomeFinderUtils.getUliKey(mobileData),
          HomeFinderUtils.getCounty(mobileData),
          HomeFinderUtils.getUnit(mobileData),
          HomeFinderUtils.getUnitSub(mobileData),
          HomeFinderUtils.getLot(mobileData),
          HomeFinderUtils.getLotSub(mobileData),
          HomeFinderUtils.getVLSNumber(mobileData),
          HomeFinderUtils.getId(mobileData),
          HomeFinderUtils.getDescription(mobileData),
          HomeFinderUtils.getListingPrice(mobileData),
          HomeFinderUtils.getVillageName(mobileData),
          HomeFinderUtils.getStreetAddress(mobileData),
          HomeFinderUtils.getNumberOfBedrooms(mobileData),
          HomeFinderUtils.getNumberOfBathrooms(mobileData),
          HomeFinderUtils.getSquareFootage(mobileData),
          HomeFinderUtils.getSeriesNumber(mobileData),
          HomeFinderUtils.getListingDate(mobileData),
          HomeFinderUtils.getIsOpenHouse(mobileData),
          HomeFinderUtils.getIsFurnished(mobileData),
          HomeFinderUtils.getCurrentPrice(mobileData),
          homeType)
        homeList.add(home)
      }
      return homeList
    }

    fun getHomeDetails(response: HomeDetailResponse) : HomeDetails {
      return HomeDetails("")
    }

    fun getFilters(response: FilterResponse) : FilterResponse {
      response.areas.sortBy{ area -> area.name }
      response.bathroomFiltrs.sortBy{ option -> option.value }
      response.bedroomFilters.sortBy{ option -> option.value }
      response.familyNeighborhoods.sortBy{ neighborhood -> neighborhood.name }
      response.garageFilters.sortBy{ value -> value.display }
      response.homesiteFilters.sortBy{ value -> value.display }
      response.seriesNew.sortBy{ option -> option.name }
      response.villages.sortBy{ village -> village.name }
      for (seriesFilter in response.series) {
        seriesFilter.options.sortBy{ option -> option.name }
      }

      return response
    }
  }
}