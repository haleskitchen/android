package com.halestorm.thevillages.data.features.retrofit

import com.halestorm.thevillages.data.features.retrofit.calendar.CalendarResponse
import com.halestorm.thevillages.data.features.retrofit.home.HomeDetailResponse
import com.halestorm.thevillages.data.features.retrofit.home.HomeResponse
import com.halestorm.thevillages.data.features.retrofit.home.ImagesResponse
import com.halestorm.thevillages.data.features.retrofit.homefilter.FilterResponse
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.net.URLEncoder

/**
 * Created by joshuahale on 1/19/18.
 */
interface VillagesApi {

  @FormUrlEncoded
  @POST("CommunityEventCalendar/Api/EventList")
  fun getCalendar(@Field("EventType") eventType : String,
                  @Field("Location") location: String,
                  @Field("FilterDate") filterDate: String,
                  @Field("SearchTag") searchTag: String,
                  @Field("SearchString") searchString: String,
                  @Field("EndDate") endDate: String,
                  @Field("StartDate") startDate: String,
                  @Field("StartRow") startRow: String,
                  @Field("EndRow") endRow: String,
                  @Field("QueryID") queryId: String) : Observable<Array<CalendarResponse>>

  @GET("vps/JsonData/agreementv2.json")
  fun getEula() : Observable<EulaResponse>

  @GET("vps/JsonData/VillagesHomefinderMobiApp.json")
  fun getHomes() : Observable<HomeResponse>

  @GET("vps/JsonData/hf_filters.json")
  fun getHomefinderFilters() : Observable<FilterResponse>

  @GET("Homefinder/Search/GeneratedDetailView")
  fun getHomeDetails(@Query("ULIKEY") uliKey: String,
                     @Query("view") view: String,
                     @Query("uliKeyArray") uliKeyArrayString: String,
                     @Query("sort") sort: String,
                     @Query("page") page: String,
                     @Query("pageLength") pageLength: String,
                     @Query("keyWords") keywords: String) : Observable<HomeDetailResponse>

  @FormUrlEncoded
  @POST("Homefinder/Search/GetFancyBoxSlidShow")
  fun getPropertyImages(@Field("county") county: String,
                        @Field("unit") unit: String,
                        @Field("unitSub") unitSub: String,
                        @Field("lot") lot: String,
                        @Field("lotSub") lotSub: String,
                        @Field("vmlNumber") vlsNumber: String) : Observable<ImagesResponse>

  companion object {
    private const val BASE_URL = "https://www.thevillages.com/"
    val HOME_DETAIL_VIEW_LIST = URLEncoder.encode("list", "UTF-8")
    val HOME_DETAIL_ULI_KEY_ARRAY = arrayOf("M218.5")
    val HOME_DETAIL_SORT = URLEncoder.encode("1,desc,0,", "UTF-8")
    val HOME_DETAIL_PAGE = URLEncoder.encode("0", "UTF-8")
    val HOME_DETAIL_PAGE_LENGTH = URLEncoder.encode("10", "UTF-8")
    val HOME_DETAIL_KEYWORDS = URLEncoder.encode("", "UTF-8")

    fun getUliKeyArrayString() : String {
      var string = "";
      for (s in HOME_DETAIL_ULI_KEY_ARRAY) {
        string += URLEncoder.encode(s, "UTF-8")
      }
      return string
    }

    fun create() : VillagesApi {
      val okhttp = OkHttpClient.Builder()
      val loggingInterceptor = HttpLoggingInterceptor()
      loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

      okhttp.addInterceptor { chain ->
          val original = chain.request()

          val authorized = original.newBuilder()
            .addHeader("Cookie", "ASPSESSIONIDQUTCQCQQ=AIFIKDPAJHDMLMFOKEEDLGFI")
            .build()

          chain.proceed(authorized)
        }
        .build()

      okhttp.addInterceptor(loggingInterceptor)

      val retrofit = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .client(okhttp.build())
        .build()

      return retrofit.create(VillagesApi::class.java)
    }
  }
}