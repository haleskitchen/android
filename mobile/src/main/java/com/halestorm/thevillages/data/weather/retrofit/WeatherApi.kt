package com.halestorm.thevillages.data.weather.retrofit

import com.halestorm.thevillages.data.weather.retrofit.response.forecast.ForecastResponse
import com.halestorm.thevillages.data.weather.retrofit.response.weather.WeatherResponse
import io.reactivex.Observable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

  @GET("weather")
  fun getWeather(@Query("id") id: Int): Observable<WeatherResponse>

  @GET("forecast")
  fun getFiveDayForecast(@Query("id") id: Int): Observable<ForecastResponse>

  companion object {
    private const val BASE_URL = "http://api.openweathermap.org/data/2.5/"
    private const val KEY_APP_ID = "APPID"
    private const val APP_ID = "7e2e4f75afcc63a7ac4da788d3997679"

    fun create(): WeatherApi {
      val okhttp = OkHttpClient.Builder()
      val interceptor = Interceptor { chain ->
        val request = chain.request().url().newBuilder().addQueryParameter(KEY_APP_ID, APP_ID).build()
        val requestBuilder = chain.request().newBuilder()?.url(request)
        chain.proceed(requestBuilder!!.build())
      }

      val loggingInterceptor = HttpLoggingInterceptor()
      loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

      okhttp.addInterceptor(loggingInterceptor)
      okhttp.networkInterceptors().add(interceptor)


      val retrofit = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .client(okhttp.build())
        .build()

      return retrofit.create(WeatherApi::class.java)
    }
  }
}