package com.halestorm.thevillages.data.features.retrofit

import com.google.gson.annotations.SerializedName

data class EulaResponse (
  @SerializedName("version") val version : String,
  @SerializedName("agreement") val agreement : String
)