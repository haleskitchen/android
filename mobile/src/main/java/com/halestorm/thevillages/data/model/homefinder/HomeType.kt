package com.halestorm.thevillages.data.model.homefinder

/**
 * Created by joshuahale on 2/8/18.
 */
enum class HomeType {
  NEW, PENDING, PREOWNED
}