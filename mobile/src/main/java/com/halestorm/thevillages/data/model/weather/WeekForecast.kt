package com.halestorm.thevillages.data.model.weather

data class WeekForecast (
  val days : ArrayList<Day>
)