package com.halestorm.thevillages.data.features.retrofit.home

import com.google.gson.annotations.SerializedName

data class HomeData (
  @SerializedName("mobiData") val mobileDataList : List<List<String>>
)