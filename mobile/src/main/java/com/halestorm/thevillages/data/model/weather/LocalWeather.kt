package com.halestorm.thevillages.data.model.weather

import android.os.Parcel
import android.os.Parcelable

data class LocalWeather(
  private var maxTemperature: Int,
  private var minTemperature: Int,
  var name: String,
  private var sunriseInMillis: Long,
  private var sunsetInMillis: Long,
  var temperature: Int
) : Parcelable {
  constructor(source: Parcel) : this(
    source.readInt(),
    source.readInt(),
    source.readString(),
    source.readLong(),
    source.readLong(),
    source.readInt()
  )

  override fun describeContents() = 0

  override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
    writeInt(maxTemperature)
    writeInt(minTemperature)
    writeString(name)
    writeLong(sunriseInMillis)
    writeLong(sunsetInMillis)
    writeInt(temperature)
  }

  companion object {
    @JvmField
    @Suppress("UNUSED_PARAMETER")
    val CREATOR: Parcelable.Creator<LocalWeather> = object : Parcelable.Creator<LocalWeather> {
      override fun createFromParcel(source: Parcel): LocalWeather = LocalWeather(source)
      override fun newArray(size: Int): Array<LocalWeather?> = arrayOfNulls(size)
    }
  }
}